﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace random_logger
{
    public partial class Form1 : Form
    {
        int lastLineFactor = 0;
        int spaces = 0;
        string fileNameSource;
        string fileNameOutput;
        public Form1()
        {
            InitializeComponent();
        }
        
      
        private void button1_Click(object sender, EventArgs e)
        {
            readTime();
        }

        public string readTime()
        {
            try
            {
                var fs = new FileStream(fileNameSource, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (var sr = new StreamReader(fs))
                {
                    string txt = sr.ReadToEnd();
                    string[] parts = txt.Split('\n');

                    string line = parts[parts.Length - lastLineFactor - 1];
                    string[] parts2 = line.Split(' ');
                    string time = parts2[spaces];
                    sr.Close();
                    return time;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "error";
            }

        }

        public void writeOutput(string event_)
        {
            StreamWriter sw = new StreamWriter(fileNameOutput, true);
            string txt = readTime() + ": " + event_;
            richTextBox1.Text = txt + "\n" + richTextBox1.Text;
            sw.WriteLine(txt);
            sw.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader("config.txt");
            lastLineFactor = Int16.Parse(sr.ReadLine().Replace("last-line-factor=", ""));
            spaces = Int16.Parse(sr.ReadLine().Replace("spaces=", ""));
            sr.Close();

            OpenFileDialog f = new OpenFileDialog();
            if(f.ShowDialog()==DialogResult.OK)
            {
                fileNameSource = f.FileName;
                fileNameOutput = Path.GetDirectoryName(fileNameSource) + "\\" + Path.GetFileNameWithoutExtension(fileNameSource) + "_events.txt";
                this.Text = "File: "+Path.GetFileName(fileNameSource);
            }
        }

        

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string txt = textBox1.Text;
                textBox1.Text = "";
                
                writeOutput(txt);
            }
        }

        private void clearFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Are you sure ?", "Delete file", MessageBoxButtons.YesNo,MessageBoxIcon.Question )==DialogResult.Yes)
            {
                File.Delete(fileNameOutput);
            }
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show( "Are you sure ?", "New file", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Restart();
            }
        }
    }
}
