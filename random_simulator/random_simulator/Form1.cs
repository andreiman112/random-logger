﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace random_simulator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            StreamWriter sw = new StreamWriter(Path.GetDirectoryName(Application.ExecutablePath) + "\\random.asc",true);
            var str = "  " + DateTime.Now.Millisecond+" RX 1 4123 123 123 123 123 123 s\n";
            sw.Write(str);
            richTextBox1.Text = str + richTextBox1.Text;
            sw.Close();
        }
    }
}
